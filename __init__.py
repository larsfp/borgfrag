#!/usr/bin/env python3

import sys
import os
import subprocess
import json
import argparse
import time

from colorama import init, Fore, Back, Style
init()

borg_bin="borg"
debug = True

class Repofile:
    def __init__(self, type = '-', mode = '', user = '', group = '',
        healthy = False, mtime = None, size = 0, change = 'new '):
        self.type = type
        self.mode = mode
        self.user = user
        self.group = group
        self.healthy = healthy
        self.mtime = mtime
        self.size = size
        self.change = change

    def __getitem__(self, property):
        if property == 'path':
            return self.path
        if property == 'user':
            return self.user
        if property == 'mtime':
            return self.mtime
        if property == 'size':
            return self.size
        if property == 'change':
            return self.change
        if property == 'type':
            return self.type
        print("Not implemented")
        sys.exit(1)

    def __setitem__(self, property, data):
        if property == 'user':
            self.user = data
        elif property == 'mtime':
            self.mtime = data
        elif property == 'size':
            self.size = data
        elif property == 'change':
            self.change = data
        else:
            print("Not implemented")
            sys.exit(1)


def get_runs(borg_repo, args="--last=2"):
    ''' Get two last runs for comparison '''
    proc = subprocess.run([borg_bin, "list", args, "--json", borg_repo], capture_output=True, check=True)
    result = json.loads(proc.stdout.decode())
    try:
        if args[-1] == "2":
            return {0: result['archives'][0], 1: result['archives'][1]}
        else:
            return {0: result['archives'][0]}
    except IndexError:
        print("Two runs not found")
        sys.exit(1)

def get_filelist(run_name):
    ''' Get list of files in a run '''

    proc = subprocess.run([
        borg_bin, 
        "list", 
        "--json-lines", 
        borg_repo + '::' + run_name
    ], capture_output=True, check=True)
    try:
        return proc.stdout.decode()
    except json.JSONDecodeError as err:
        print(err)
        with open('bad.json', mode = 'w', encoding='UTF-8') as f:
            f.write(proc.stdout.decode())
    return False

def parse_first(run):
    ''' Parse first run (or previous run if running backup) '''
    repo_filelist = {}

    json_Repofile = get_filelist(run["name"])
    for jf in json_Repofile.splitlines():
        fileinfo = json.loads(jf)
        if type == 'd':
            continue
        path = fileinfo['path']

        #insert
        repo_filelist[path] = Repofile(
            type = fileinfo['type'], mode = fileinfo['mode'],
            user = fileinfo['user'], group =  fileinfo['group'],
            healthy = fileinfo['healthy'], mtime =  fileinfo['mtime'],
            size =  fileinfo['size'], change=''
        )
    return repo_filelist

def parse_second(run, prev_run):
    ''' Compare file list from two runs '''

    repo_filelist = {}
    json_Repofile = get_filelist(run["name"])

    for jf in json_Repofile.splitlines():
        fileinfo = json.loads(jf)
        type = fileinfo['type']
        if type == 'd':
            continue
        mode = fileinfo['mode']
        user = fileinfo['user']
        group = fileinfo['group']
        uid = fileinfo['uid']
        gid = fileinfo['gid']
        path = fileinfo['path']
        healthy = fileinfo['healthy']
        source = fileinfo['source']
        linktarget = fileinfo['linktarget']
        flags = fileinfo['flags']
        size = fileinfo['size']
        mtime = fileinfo['mtime']

        repo_filelist[path] = Repofile(
            type, mode,
            user, group,
            healthy, mtime,
            size, change = 'new '
        )

        if path in prev_run.keys(): # Existed
            if size != prev_run[path]['size']:
                print(Back.GREEN + 'S' + Style.RESET_ALL, end='')
            elif mtime != prev_run[path]['mtime']:
                print(Back.GREEN + 'M' + Style.RESET_ALL, end='')
            else:
                print(Fore.BLUE + 'K' + Style.RESET_ALL, end='')

        else: # File was new in this run
            print(Fore.GREEN + 'A' + Style.RESET_ALL, end='')

        if debug:
            print("", path)

    # Check for deleted files
    for path in prev_run.keys():
        if prev_run[path]['type'] == 'd':
            continue
        if path not in repo_filelist.keys():
            print(Fore.RED + 'R' + Style.RESET_ALL, end='')
            if debug:
                print("", path)

    print()

def create_run(prev_run, borg_repo, target_path):
    ''' Create a borg run while comparing to previous run '''

    repo_filelist = {}
    command = [borg_bin, "create", "--list", "--log-json",
        borg_repo + "::" + time.strftime("%Y.%m.%d_%H:%M:%S"),
        target_path]
    if debug:
        print(command)

    process = subprocess.Popen(command, stdout=subprocess.PIPE, 
        stderr=subprocess.STDOUT, encoding='utf-8')
    while True:
        output = process.stdout.readline()
        if output == '' and process.poll() is not None:
            break
        if output:
            if debug:
                print(output)
            try:
                jl = json.loads(output.strip())
            except json.JSONDecodeError:
                continue

            if jl["status"] == 'd':
                continue

            repo_filelist[jl['path']] = Repofile(
                type=jl["status"], change = 'new '
            )

            if jl["status"] == 'U':
                print(Back.GREEN + 'U' + Style.RESET_ALL, end='')
            elif jl["status"] == 'A': # File was new in this run
                print(Fore.GREEN + 'A' + Style.RESET_ALL, end='')
            else:
                print("?")

            if debug:
                print("", jl['path'])

    rc = process.poll()
    #return rc

    if process.returncode != 0:
        print("Error running %, error %s" % (''.join(command), process.returncode))

    # Check for deleted files
    for path in prev_run.keys():
        if prev_run[path]['type'] == 'd':
            continue
        if path not in repo_filelist.keys():
            print(Fore.RED + 'R' + Style.RESET_ALL, end='')
            if debug:
                print("", path)

    print()

def validate_operation(string):
    ''' Validate operation '''
    if string in ['create', '']:
        return string
    else:
        raise InvalidOperation(string)

def dir_path(string):
    ''' Validate it's a directory '''
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)

def parse_args():
    parser = argparse.ArgumentParser(description='Visualize borgbackup runs')
    parser.add_argument('repo_path', type=dir_path, nargs=1, help='Path to the backup repo')
    # parser.add_argument('operation', type=validate_operation, nargs='?',
    #     help='[create] If set, create a backup run. Else show last diff', default='')
    parser.add_argument('path', type=dir_path, nargs='?', default='',
        help='[path] If set, a new backup will be created. Else a diff of last two backups are shown.')
    args = parser.parse_args()
    return args.repo_path[0], args.path

if __name__=="__main__":
    borg_repo, target_path = parse_args()
    
    if not target_path:
        get_run_args="--last=2"
    else:
        get_run_args="--last=1"
    runs = get_runs(borg_repo, args=get_run_args)
    repo_filelist = parse_first(runs[0])

    if not target_path:
        parse_second(runs[1], prev_run=repo_filelist)
    else:
        create_run(prev_run=repo_filelist, borg_repo=borg_repo, target_path=target_path)

