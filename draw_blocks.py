from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt, QTimer, QDateTime
import time
import sys

time = QtCore.QTime(0, 0, 0)

class Block(QtWidgets.QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setWindowTitle('QT')

        self.timer=QTimer()
        self.timer.timeout.connect(self.showTime)
        self.startTimer()

    def paintEvent(self, e):
        painter = QtGui.QPainter(self)

        brush = QtGui.QBrush()
        brush.setColor(QtGui.QColor('white'))
        brush.setStyle(Qt.SolidPattern)
        rect = QtCore.QRect(0, 0, painter.device().width(), painter.device().height())
        painter.fillRect(rect, brush)
        padding = 10
        block_width = 50
        block_height = 70
        block_padding = 15
        block_border = 4

        # Define our canvas.
        d_height = painter.device().height() - (padding * 2)
        d_width = painter.device().width() - (padding * 2)

        time=QDateTime.currentDateTime()
        seconds=int(time.toString('ss'))

        # Draw the bars.
        y_offset = padding

        for count in range(0, seconds):
            x_offset = (count*(padding+block_width)) % (painter.device().width() - block_width)
            y_lines = count*(padding+block_width) // (painter.device().width() - block_width)
            y_offset = (y_lines*(padding+block_height))

            # Draw block with cyan fill
            rect = QtCore.QRect(
                padding+x_offset,
                padding+y_offset,
                block_width,
                block_height)
            painter.setPen(QtGui.QPen(Qt.black, block_border, Qt.SolidLine))
            painter.setBrush(QtGui.QBrush(Qt.cyan, Qt.SolidPattern))
            painter.drawRect(rect)

        painter.end()

    def _trigger_refresh(self):
        self.update()

    def showTime(self):
        time=QDateTime.currentDateTime()
        timeDisplay=time.toString('yyyy-MM-dd hh:mm:ss dddd')
        print(timeDisplay)
        print(time.toString('ss'))
        self.update()

    def startTimer(self):
        self.timer.start(1000)
        print("start")

    def endTimer(self):
        self.timer.stop()
        print("stop")


if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    b = Block()
    b.show()
    sys.exit(app.exec_())
