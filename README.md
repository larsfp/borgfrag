Borg fragments
==============

A prototype. Should be ported to a borg GUI if it makes sense.

Idea
----

- To make people do backups, make backups more visualy interesting.

I've spent some time in my life looking at the Windows 95 defragment tool. Can the same visual effect be used to make backing up as pleasing as defragmenting?

Step 1: visualize diffs between borg runs, cli.
Step 2: visualize the log/diff while running, cli.
Step 3: visualize the log/diff while running, in some test-GUI.
Step 4: visualize the log/diff while running, like win95 but modernized?
Step 5: include in an existing borg GUI

Inspiration
-----------

* http://hultbergs.org/defrag/


Commands
--------

https://borgbackup.readthedocs.io/en/stable/internals/frontends.html#file-listings

    $ borg list --json-lines  test::"on."|less -iS
    $ borg diff --json-lines test::"on." "on. 15. sep. 22:29:25 +0200 2021"|less -iS
    $ borg create test::"$(date)" /tmp

Perhaps use?
------------

* https://github.com/spslater/borgapi

TODO
----

* Visualize excludes?

Output
------

v.1 (colors not shown)

    $ python3 __init__.py 
    RKKKKKKKKKKKKKKKKKKKKCKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKRRKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKRR

Screenshots
-----------

Testing qt5 for drawing blocks

![Drawtest](img/drawtest.png)
